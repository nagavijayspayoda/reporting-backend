package org.ishafoundation.reports;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.google.common.base.Predicate;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

@Configuration
@EnableSwagger2
//@ComponentScan(basePackages = { "io.swagger.controller" })
public class SwaggerConfig {
	
	 @Bean
	    public WebMvcConfigurer corsConfigurer() {
	        return new WebMvcConfigurerAdapter() {
	            @Override
	            public void addCorsMappings(CorsRegistry registry) {
	            	registry.addMapping("/**");
	            }
	        };
	    }
	// @Override
	// public void addResourceHandlers(ResourceHandlerRegistry registry) {
	//
	// registry
	// .addResourceHandler("**/**")
	// .addResourceLocations("classpath:/META-INF/resources/");
	// registry
	// .addResourceHandler("**/webjars/**")
	// .addResourceLocations("classpath:/META-INF/resources/webjars/");
	//
	// }

	/*@Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("io.swagger.controller"))
                .paths(regex("/api/v1/*"))
                .build()
                .apiInfo(metaData());
    }*/

//	private ApiInfo metaData() {
//		ApiInfo apiInfo = new ApiInfo("Report Builder API", "Report Builder Server API", "1.0",
//				"Terms of service",
//				new Contact("Spring user", "https://example.com", "springuser@spring.com"),
//				"Proprietary", "Proprietary License");
//		return apiInfo;
//	}
//	
//	@Bean
//	public Docket postsApi() {
//		return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
//				.apiInfo(metaData()).select().paths(regex(".*")).build();
//	}

	private Predicate<String> postPaths() {
		return or(regex("/api/.*"));
	}
	
	/*@Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }*/
}
