package org.ishafoundation.reports.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-01-22T11:57:12.556Z")

@Configuration
public class SwaggerDocumentationConfig {

    
	private ApiInfo metaData() {
		ApiInfo apiInfo = new ApiInfo("Report Builder API", "Report Builder Server API", "1.0",
				"Terms of service",
				new Contact("Nagavijay", "https://demo-central-server.com", "nagavijay.sivakumar@ishafoundation.org"),
				"Proprietary", "Proprietary License");
		return apiInfo;
	}

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("org.ishafoundation.reports.controller"))
                    .build()
                .directModelSubstitute(org.joda.time.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(org.joda.time.DateTime.class, java.util.Date.class)
                .apiInfo(metaData());
    }

}
