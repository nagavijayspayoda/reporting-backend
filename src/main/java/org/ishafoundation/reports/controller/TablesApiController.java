package org.ishafoundation.reports.controller;

import io.swagger.annotations.*;

import org.ishafoundation.reports.api.TablesApi;
import org.ishafoundation.reports.model.Error;
import org.ishafoundation.reports.model.ResponseData;
import org.ishafoundation.reports.model.Table;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-01-22T11:57:12.556Z")

@RestController
public class TablesApiController implements TablesApi {

    public ResponseEntity<List<Table>> tablesGet() {
    	List<Table> tableList = populateTableList();
		
        return new ResponseEntity<List<Table>>(tableList, HttpStatus.OK);
    }

    @RequestMapping(value = "/tablesnew",
            produces = { "application/json" }, 
            method = RequestMethod.GET)
    public ResponseEntity<ResponseData> tablesNewGet() {
    	ResponseData data = new ResponseData();
    	
    	
    	List<Table> tableList = populateTableList();
    	data.setData(tableList);
    	
    	return new ResponseEntity<ResponseData>(data, HttpStatus.OK);
    }

	private List<Table> populateTableList() {
		List<Table> tableList = new ArrayList<Table>();
		tableList.add(new Table().tableId("Product").tableName("Product"));
		tableList.add(new Table().tableId("Order").tableName("Order"));
		tableList.add(new Table().tableId("Address").tableName("Address"));
		tableList.add(new Table().tableId("Category").tableName("Category"));
		tableList.add(new Table().tableId("Customer").tableName("Customer"));
		return tableList;
	}

}
