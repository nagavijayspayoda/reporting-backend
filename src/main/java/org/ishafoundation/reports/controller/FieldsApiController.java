package org.ishafoundation.reports.controller;

import io.swagger.annotations.*;

import org.ishafoundation.reports.api.FieldsApi;
import org.ishafoundation.reports.model.Error;
import org.ishafoundation.reports.model.Field;
import org.ishafoundation.reports.model.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.*;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-01-22T11:57:12.556Z")

@RestController
@RequestMapping(name="/reports")
public class FieldsApiController implements FieldsApi {
	
	private HashMap<String, List<Field>> tableFieldMapping;
	
	@PostConstruct
	public void init() {
		tableFieldMapping = new HashMap<String, List<Field>>();
		tableFieldMapping.put("Product", populateProductList());
		tableFieldMapping.put("Order", populateOrderList());
		tableFieldMapping.put("Address", populateAddressList());
		tableFieldMapping.put("Category", populateCategoryList());
		tableFieldMapping.put("Customer", populateCustomerList());
		
	}


	public ResponseEntity<ResponseData> fieldsGet(
			@ApiParam(value = "selected table name") @RequestParam(value = "tableName", required = false) String tableName) {
		
		List<Field> fieldList = tableFieldMapping.get(tableName);
		ResponseData responseData = new ResponseData();
		responseData.setData(fieldList);
		return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
	}
	
	
	private List<Field> populateProductList() {
		List<Field> productFields = new ArrayList<Field>();
		
		productFields.add(new Field().fieldId("1").fieldName("Product Id"));
		productFields.add(new Field().fieldId("2").fieldName("Product Name"));
		productFields.add(new Field().fieldId("3").fieldName("Category"));
		productFields.add(new Field().fieldId("4").fieldName("Units in stock"));
		productFields.add(new Field().fieldId("5").fieldName("Unit price"));
		productFields.add(new Field().fieldId("6").fieldName("Units on order"));
		productFields.add(new Field().fieldId("7").fieldName("Reorder level"));
		
		return productFields;
	}

	private List<Field> populateOrderList() {
		List<Field> orderList = new ArrayList<Field>();
		
		orderList.add(new Field().fieldId("1").fieldName("Order Id"));
		orderList.add(new Field().fieldId("2").fieldName("Customer"));
		orderList.add(new Field().fieldId("3").fieldName("Order Date"));
		orderList.add(new Field().fieldId("4").fieldName("Ship Address"));
		orderList.add(new Field().fieldId("5").fieldName("Shipped date"));
		orderList.add(new Field().fieldId("6").fieldName("Freight provider"));
		return orderList;
	}
	
	private List<Field> populateAddressList() {
		List<Field> addressFields = new ArrayList<Field>();
		
		addressFields.add(new Field().fieldId("1").fieldName("Address Id"));
		addressFields.add(new Field().fieldId("2").fieldName("Name"));
		addressFields.add(new Field().fieldId("3").fieldName("Street Name"));
		addressFields.add(new Field().fieldId("4").fieldName("City"));
		addressFields.add(new Field().fieldId("5").fieldName("Postalcode"));
		addressFields.add(new Field().fieldId("6").fieldName("Country"));
		
		return addressFields;
	}
	private List<Field> populateCategoryList() {
		List<Field> categoryFields = new ArrayList<Field>();
		
		categoryFields.add(new Field().fieldId("1").fieldName("Category Id"));
		categoryFields.add(new Field().fieldId("2").fieldName("Category Name"));
		categoryFields.add(new Field().fieldId("3").fieldName("Description"));
		categoryFields.add(new Field().fieldId("4").fieldName("Picture"));
		
		return categoryFields;
	}
	private List<Field> populateCustomerList() {
		List<Field> customerFields = new ArrayList<Field>();
		
		customerFields.add(new Field().fieldId("1").fieldName("Customer Id"));
		customerFields.add(new Field().fieldId("2").fieldName("Customer Name"));
		customerFields.add(new Field().fieldId("3").fieldName("Phone"));
		customerFields.add(new Field().fieldId("4").fieldName("Email"));
		customerFields.add(new Field().fieldId("5").fieldName("Address"));
		
		return customerFields;
	}
	

}
