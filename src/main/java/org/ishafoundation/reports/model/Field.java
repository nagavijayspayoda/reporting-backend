package org.ishafoundation.reports.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Field
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-01-22T11:57:12.556Z")

public class Field   {
  @JsonProperty("fieldId")
  private String fieldId = null;

  @JsonProperty("fieldName")
  private String fieldName = null;

  public Field fieldId(String fieldId) {
    this.fieldId = fieldId;
    return this;
  }

   /**
   * field Id
   * @return fieldId
  **/
  @ApiModelProperty(value = "field Id")


  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  public Field fieldName(String fieldName) {
    this.fieldName = fieldName;
    return this;
  }

   /**
   * field names
   * @return fieldName
  **/
  @ApiModelProperty(value = "field names")


  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Field field = (Field) o;
    return Objects.equals(this.fieldId, field.fieldId) &&
        Objects.equals(this.fieldName, field.fieldName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fieldId, fieldName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Field {\n");
    
    sb.append("    fieldId: ").append(toIndentedString(fieldId)).append("\n");
    sb.append("    fieldName: ").append(toIndentedString(fieldName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

