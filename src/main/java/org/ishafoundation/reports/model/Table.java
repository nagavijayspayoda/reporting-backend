package org.ishafoundation.reports.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Table
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-01-22T11:57:12.556Z")

public class Table   {
  @JsonProperty("tableId")
  private String tableId = null;

  @JsonProperty("tableName")
  private String tableName = null;

  public Table tableId(String tableId) {
    this.tableId = tableId;
    return this;
  }

   /**
   * id of table.
   * @return tableId
  **/
  @ApiModelProperty(value = "id of table.")


  public String getTableId() {
    return tableId;
  }

  public void setTableId(String tableId) {
    this.tableId = tableId;
  }

  public Table tableName(String tableName) {
    this.tableName = tableName;
    return this;
  }

   /**
   * Name of the table.
   * @return tableName
  **/
  @ApiModelProperty(value = "Name of the table.")


  public String getTableName() {
    return tableName;
  }

  public void setTableName(String tableName) {
    this.tableName = tableName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Table table = (Table) o;
    return Objects.equals(this.tableId, table.tableId) &&
        Objects.equals(this.tableName, table.tableName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tableId, tableName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Table {\n");
    
    sb.append("    tableId: ").append(toIndentedString(tableId)).append("\n");
    sb.append("    tableName: ").append(toIndentedString(tableName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

