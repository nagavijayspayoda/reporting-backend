# README #

This is **work in progress** reporting backend component built using Java/SpringBoot.

### What is this repository for? ###

* This is central server various applications to store reporting configuration and manage the usability of reports for various applications.

## To Run 

Run `java -jar target/reporting-backend.jar` on command prompt. Navigate to `http://localhost:9000/v1/swagger-ui.html`. 
